<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model{

	protected $table = 'productos';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'nombre', 
		'categoria', 
		'caracteristicas', 
		'cantidad',
		'condicion',
		'garantía',
		'precios_venta', 
		'precios_compra', 
		'imagen_principal', 
		'imagen_secundaria', 
		'imagen_tercera', 
		'video_id',
	];


	/** un curso puede pertenecer a un usuario	*/
	public function user(){
		return $this->belongsTo('App\User');
	}
    
}
