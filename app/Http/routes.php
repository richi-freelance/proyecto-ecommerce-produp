<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});



/* ===========================================
	Ejemplo de rutas

http://localhost:8080/users/login
=========================================== */

//mapeamos el controlador UserController
Route::controller('users', 'UserController');

Route::group(array('middleware' => ['auth']), function($group){

	Route::controller('productos', 'ProductosController');
});


Route::auth();

Route::controller('home', 'HomeController');
