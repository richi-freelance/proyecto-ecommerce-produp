<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Producto;
use App\User;
use App\Http\Requests\ProductoForm;
use DB;
use Auth;


class ProductosController extends Controller{

	/*
	* retrieve and display all productos with user
	*/
	public function getAll(){

		return view("productos.list")->with(
			'productos', 
			Producto::where("user_id", Auth::user()->id)
				->with("user")->paginate(4)->setPath('all')
		);
	}

	/*
	* display form productos
	*/
	public function getCreate(){
		return view("productos.create");
	}

	/*
	* crea un producto nuevo
	*/
	public function postCreate(Request $request, ProductoForm $productoForm){

		$this->validate($request, [
			'imagen_principal' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		$producto = new Producto;

		$producto->user_id = Auth::user()->id;

		$producto->nombre = \Request::input('nombre');
		$producto->categoria = \Request::input('categoria');
		$producto->caracteristicas = \Request::input('caracteristicas');
		$producto->cantidad = \Request::input('cantidad');
		$producto->condicion = \Request::input('condicion');
		$producto->garantía = \Request::input('garantía');
		$producto->precios_venta = \Request::input('precios_venta');
		$producto->precios_compra = \Request::input('precios_compra');

		if($file = $request->hasFile('imagen_principal')) {
			$file = $request->file('imagen_principal') ;
			$fileName = $file->getClientOriginalName() ;
			
			$destinationPath = public_path().'/storage/'.Auth::user()->id.'/';
			$file->move($destinationPath, $fileName);
			$producto->imagen_principal = $fileName;
		}

		if($file = $request->hasFile('imagen_secundaria')) {
			$file = $request->file('imagen_secundaria') ;
			$fileName = $file->getClientOriginalName() ;
			
			$destinationPath = public_path().'/storage/'.Auth::user()->id.'/';
			$file->move($destinationPath, $fileName);
			$producto->imagen_secundaria = $fileName;
		}

		if($file = $request->hasFile('imagen_tercera')) {
			$file = $request->file('imagen_tercera') ;
			$fileName = $file->getClientOriginalName() ;
			
			$destinationPath = public_path().'/storage/'.Auth::user()->id.'/';
			$file->move($destinationPath, $fileName);
			$producto->imagen_tercera = $fileName;
		}

		$producto->video_id = \Request::input('video_id');
		$producto->save();

		\Session::flash('producto_created', \Lang::get("messages.producto_created"));
		return redirect()->back();
	}




	public function getEdit($id){

		$producto = Producto::find($id);

		if($producto){
			return view("productos.edit")->with('producto', $producto);
		}
		return redirect()->back();
	}

	public function postEdit(Request $request, ProductoForm $productoForm, $id){

		$producto = Producto::find($id);


		$this->validate($request, [
			'imagen_principal' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);

		$producto->user_id = Auth::user()->id;

		$producto->nombre = \Request::input('nombre');
		$producto->categoria = \Request::input('categoria');
		$producto->caracteristicas = \Request::input('caracteristicas');
		$producto->cantidad = \Request::input('cantidad');
		$producto->condicion = \Request::input('condicion');
		$producto->garantía = \Request::input('garantía');
		$producto->precios_venta = \Request::input('precios_venta');
		$producto->precios_compra = \Request::input('precios_compra');

		if($file = $request->hasFile('imagen_principal')) {
			$file = $request->file('imagen_principal') ;
			$fileName = $file->getClientOriginalName() ;
			
			$destinationPath = public_path().'/storage/'.Auth::user()->id.'/';
			$file->move($destinationPath, $fileName);
			$producto->imagen_principal = $fileName;
		}

		if($file = $request->hasFile('imagen_secundaria')) {
			$file = $request->file('imagen_secundaria') ;
			$fileName = $file->getClientOriginalName() ;
			
			$destinationPath = public_path().'/storage/'.Auth::user()->id.'/';
			$file->move($destinationPath, $fileName);
			$producto->imagen_secundaria = $fileName;
		}

		if($file = $request->hasFile('imagen_tercera')) {
			$file = $request->file('imagen_tercera') ;
			$fileName = $file->getClientOriginalName() ;
			
			$destinationPath = public_path().'/storage/'.Auth::user()->id.'/';
			$file->move($destinationPath, $fileName);
			$producto->imagen_tercera = $fileName;
		}

		$producto->video_id = \Request::input('video_id');
		$producto->save();

		\Session::flash('producto_updated', \Lang::get("messages.producto_updated"));
		return redirect()->back();
	}

	public function deleteDestroy($id){

		$producto = Producto::find($id);
		
		if($producto){
			$producto->delete();
			\Session::flash('producto_deleted', \Lang::get("messages.producto_deleted"));
		}
		return redirect()->back();
	}


}
