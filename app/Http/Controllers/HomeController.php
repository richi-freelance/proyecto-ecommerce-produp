<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Producto;
use App\User;
use DB;
use Auth;


class HomeController extends Controller{

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(){
        
        return view("home")->with('productos', Producto::with("user")->paginate(4)->setPath('all'));
    }
}
