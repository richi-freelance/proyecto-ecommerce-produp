<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductoForm extends Request{

	
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(){

		return true; // activa autorizacion
	}

	/**
	 * 
	 *            'imagen_principal' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

	 * @return rules
	 */
	public function rules(){

		return [
            'nombre' => 'required',
            'categoria' => 'required',
        ];

	}


}
