<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::create('productos', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            $table->integer('user_id')->unsigned(); 

            $table->string('nombre', 60);
            $table->string('categoria', 60);
            $table->string('caracteristicas', 100);
            $table->integer('cantidad');
            $table->string('condicion', 100);
            $table->string('garantía', 100);

            $table->float('precios_venta', 8, 2);  
            $table->float('precios_compra', 8, 2);  
            $table->string('imagen_principal');
            $table->string('imagen_secundaria');
            $table->string('imagen_tercera');

            $table->string('video_id');
  
            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::drop('productos');
    }
}
