@extends('layouts.app')

@section('content')



@if(sizeof($productos) > 0)
    @foreach($productos as $index => $producto)
        
        <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
                
                <!-- ==================================================
                    Aqui recuperamos la url de la imagen 

                aunque la guarda,  con nombres aleatorios, no con el nombre original  
                ================================================== -->
                <img src="/storage/{{$producto->user_id}}/{{$producto->imagen_principal}}" />
                
                <div class="caption">
                    <h3>{{$producto->nombre}}</h3>
                    <p>{{$producto->categoria}}</p>  
                    <p>{{$producto->caracteristicas}}</p> 
                </div>

            </div>
        </div>

    @endforeach

@else
    <div class="alert alert-danger">
        <p>Al parecer este album no tiene productos. Crea una.</p>
    </div>
@endif



<div class="row">
    <div class="center">
        
            <?php echo $productos->render(); ?>

    </div>
</div>


@endsection
