@extends('layouts.app')

@section('title')

	@lang('messages.edit_course')

@endsection

@section('sidebar')

	@include('includes/sidebar')

@endsection

@section('content')

		<div class="col-md-10 col-md-offset-1">
		@if (Session::has('producto_updated'))
			<div class="alert alert-success">{!! Session::get('producto_updated') !!}</div>
		@endif

		<h1 class="text-muted text-center">@lang('messages.edit_producto')</h1>

		{{-- Incluimos el template que muestra errores --}}
		@include('includes/errors')

			<div class="form-group">

				{!! Form::open(['url' => array('producto/edit', $producto->id), 'class' => 'form']) !!}

					{!! Form::label('nombre', Lang::choice('messages.nombre', 1)) !!}
					{!! Form::text('nombre', 
						old('nombre') ? old('nombre') : $producto->nombre, ["class" => "form-control"]) !!}<br>

					{!! Form::label('categoria', Lang::choice('messages.categoria', 1)) !!}
					{!! Form::text('categoria', 
						old('categoria') ? old('categoria') : $producto->categoria, ["class" => "form-control"]) !!}<br>

					{!! Form::label('caracteristicas', Lang::choice('messages.caracteristicas', 1)) !!}
					{!! Form::text('caracteristicas', 
						old('caracteristicas'), ["class" => "form-control"]) !!}<br>

					{!! Form::label('cantidad', Lang::choice('messages.cantidad', 1)) !!}
					{!! Form::text('cantidad', 
						old('cantidad') ? old('cantidad') : $producto->cantidad, ["class" => "form-control"]) !!}<br>

					{!! Form::label('condicion', Lang::choice('messages.condicion', 1)) !!}
					{!! Form::text('condicion', 
						old('condicion') ? old('condicion') : $producto->condicion, ["class" => "form-control"]) !!}<br>

					{!! Form::label('garantía', Lang::choice('messages.garantía', 1)) !!}
					{!! Form::text('garantía', 
						old('garantía') ? old('garantía') : $producto->garantía, ["class" => "form-control"]) !!}<br>

					{!! Form::label('precios_venta', Lang::choice('messages.precios_venta', 1)) !!}
					{!! Form::text('precios_venta', 
						old('precios_venta') ? old('precios_venta') : $producto->precios_venta, ["class" => "form-control"]) !!}<br>

					{!! Form::label('precios_compra', Lang::choice('messages.precios_compra', 1)) !!}
					{!! Form::text('precios_compra', 
						old('precios_compra') ? old('precios_compra') : $producto->precios_compra, ["class" => "form-control"]) !!}<br>

					{!! Form::label('imagen_principal', Lang::choice('messages.imagen_principal', 1)) !!}
					{!! Form::file('imagen_principal', ["class" => "field"]) !!}<br>

					{!! Form::label('imagen_secundaria', Lang::choice('messages.imagen_secundaria', 1)) !!}
					{!! Form::file('imagen_secundaria', ["class" => "field"]) !!}<br>

					{!! Form::label('imagen_tercera', Lang::choice('messages.imagen_tercera', 1)) !!}
					{!! Form::file('imagen_tercera', ["class" => "field"]) !!}<br>


					{!! Form::label('video_id', Lang::choice('messages.video_id', 1)) !!}
					{!! Form::text('video_id', 
						old('precios_compra') ? old('precios_compra') : $producto->precios_compra, ["class" => "form-control"]) !!}<br>

					<br />
					{!! Form::submit(Lang::get('messages.edit_course'), ["class" => "btn btn-success btn-block"]) !!}

				{!! Form::close() !!}

			</div>

		</div>

@endsection
