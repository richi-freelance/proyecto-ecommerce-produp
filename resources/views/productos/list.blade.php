@extends('layouts.app')

@section('title')

	@lang('messages.login')

@endsection

@section('sidebar')

	@include('includes/sidebar')

@endsection

@section('content')



<div class="row">
    <div class="center">
		
		{!! Html::link(url('productos/create'), 
			\Lang::get('messages.create_producto'), array('class' => 'btn btn-primary btn-lg btn-block')) !!}	

    </div>
</div>


@if(sizeof($productos) > 0)
	@foreach($productos as $index => $producto)
		
		<div class="col-sm-6 col-md-3">
			<div class="thumbnail">
				
				<!-- ==================================================
					Aqui recuperamos la url de la imagen 

				aunque la guarda,  con nombres aleatorios, no con el nombre original  
				================================================== -->
				<img src="/storage/{{$producto->user_id}}/{{$producto->imagen_principal}}" />
				
				<div class="caption">
					<h3>{{$producto->nombre}}</h3>
					<p>{{$producto->categoria}}</p>  
					<p>{{$producto->caracteristicas}}</p> 
				</div>
				<p>
					{!! Html::link(url('productos/edit', $producto->id), 
						\Lang::get('messages.edit'), array('class' => 'btn btn-success btn-xs')) !!}
				</p>
				<p>					  
				  {!! Form::open(array('url' => array('productos/destroy', $producto->id), 'method' => 'DELETE')) !!}
					  <button type="submit" class="btn btn-danger btn-xs">@lang('messages.delete')</button>
				  {!! Form::close() !!}
				</p>
			

			</div>
		</div>

	@endforeach

@else
	<div class="alert alert-danger">
		<p>Al parecer este album no tiene productos. Crea una.</p>
	</div>
@endif



<div class="row">
    <div class="center">
		
			<?php echo $productos->render(); ?>

    </div>
</div>


@endsection
