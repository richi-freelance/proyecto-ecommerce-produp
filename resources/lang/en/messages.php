<?php

return [
    "welcome"               =>  "Welcome to Laravel 5 :user", //parámetros trans
   
    "not_autorizate_delete"=>  "No esta autorizado a eliminar",    
    "not_autorizate_updated"=>  "No esta autorizado a modificar",
    "login"                 =>  "Iniciar sesión",
    "password"              =>  "Contraseña",
    "email"                 =>  "Email",
    "wrong_credentials"     =>  "Los datos de acceso son incorrectos",
    "name"                  =>  "Nombre",
    "passwordconf"          =>  "Confirma el password",
    "register"              =>  "Registro",
    "success_register"      =>  "Te has registrado satisfactoriamente",
    "profile"               =>  "Mi Perfil",
    "edit_profile"               =>  "Editar mi Perfil",
    "posts"                 =>  "Posts",
    "add_producto"            =>  "Crear producto",
    "productos"               =>  "producto|productos", //pluralizaciones @choice
    "users"                 =>  "Usuarios",
    "list_users"            =>  "Listar usuarios",
    "create_user"           =>  "Crear usuario",
    "list_posts"            =>  "Listar posts",
    "create_post"           =>  "Crear post",
    "list_productos"           =>  "Listar productos",
    "create_producto"          =>  "Crear producto",
    "productos_users"          =>  "productos => Usuarios",
    "edit"                  =>  "Editar",
    "delete"                =>  "Eliminar",
    "detail"                =>  "Detalle",
    "destroy"                =>  "Eliminar",

    "without_productos"       =>  "Sin productos",
    
    "update_profile"        =>  "Actualizar perfil",
    "success_profile"       =>  "Perfil actualizado correctamente",
    "user_updated"          =>  "Usuario actualizado",
    "update_user"           =>  "Actualizar usuario",
    
    
    "photo"                 =>  "Añadir foto",
    "add_post"              =>  "Añadir post",
    "title"                 =>  "Título",
    "content"               =>  "Contenido",
    "author"                =>  "Autor",
    
    "post_created"          =>  "El post se ha creado correctamente",
    "edit_post"             =>  "Editar post",
    "post_updated"          =>  "El post ha sido actualizo correctamente",
    "post_deleted"          =>  "El post ha sido eliminado correctamente",
    "withoutposts"          =>  "No hay posts",
    "withoutproductos"        =>  "No hay productos",
    "withoutusers"          =>  "No hay usuarios",
    "edit_producto"           =>  "Editar producto",
    
    "producto_created"        =>  "El producto ha sido creado correctamente",
    "producto_updated"        =>  "El producto ha sido actualizo correctamente",
    "producto_deleted"        =>  "El producto ha sido eliminado correctamente",
    "producto_related"        =>  "El producto ha sido relacionado",
    
    "subject"               =>  "Motivo",
    "comment"               =>  "Comentario",
    "add_comment"           =>  "Añadir comentario",
    "user_deleted"          =>  "El usuario ha sido eliminado correctamente",
    "comment_saved"         =>  "El comentario se ha guardado correctamente",
    "add_user"              =>  "Añadir usuario",
    "subscribe"             =>  "Apuntarme",
    "subscribed"            =>  "Apuntado",

    "language"              =>  "Idioma",
    "spanish"               =>  "Español",
    "english"               =>  "Inglés",
    "logout"                =>  "Cerrar sesión"
];
